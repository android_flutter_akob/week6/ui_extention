import 'package:flutter/material.dart';
import 'package:ui_extention/checkbox_widget.dart';
import 'package:ui_extention/dropdown_widget.dart';
import 'package:ui_extention/radio_widget.dart';

import 'checkboxtile_widget.dart';

void main() {
  runApp(
    MaterialApp(title: 'Ui Extention', home: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ui Extention'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('Ui Menu'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('CheckBoxitle'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTileWidget()));
            },
          ),
          ListTile(
            title: Text('DropDown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWiget()));
            },
          ),
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('CheckBoxitle'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTileWidget()));
            },
          ),
          ListTile(
            title: Text('DropDown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWiget()));
            },
          ),
        ],
      ),
    );
  }
}
